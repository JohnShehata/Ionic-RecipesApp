import { SigninPage } from './../pages/signin/signin';
import { TabsPage } from './../pages/tabs/tabs';
import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SignupPage } from '../pages/signup/signup';
import firebase from 'firebase';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  //rootPage:any = TabsPage;
  TabsPage:any=TabsPage;
  SignInPage:any=SigninPage;
  SignUpPage:any=SignupPage;
  @ViewChild('nav') Nav:NavController;
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,private MenuCntrl:MenuController) {
    firebase.initializeApp({
      apiKey: "AIzaSyAX3-InZu8A8_cV-OgfFLLkKIXPlQInduI",
      authDomain: "ionic2-recipebook-f4eec.firebaseapp.com",
    });
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  OnLoad(SelectedPage:any)
  {
    this.Nav.setRoot(SelectedPage);
    this.MenuCntrl.close();
  }
  OnLogout()
  {
    
  }
}

