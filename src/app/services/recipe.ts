import { Ingredient } from './../../models/ingredients';
import { Recipe } from './../../models/recipe';
export class RecipeService
{
    private Recipes:Recipe[]=[];

    AddItem(title:string,description:string,difficulty:string,Ingredients:Ingredient[])
    {
        this.Recipes.push(new Recipe(title,description,difficulty,Ingredients));
    }
    AddItems(Items:Recipe[])
    {
            this.Recipes.push(...Items);
    }
    RemoveItem(Item:Recipe)
    {
        const Index=this.Recipes.findIndex((itemElm)=>{
                return Item.title==itemElm.title;
        })
        this.Recipes.splice(Index,1);
    }

    GetRecipesList()
    {
        return this.Recipes.slice();
    }

    UpdateRecipe(index:number,title:string,description:string,difficulty:string,Ingredients:Ingredient[])
    {
        this.Recipes[index]=new Recipe(title,description,difficulty,Ingredients);
    }
}