import { AuthService } from './../../app/services/auth';
import { NgForm } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private AuthService:AuthService,
    private LoadingCntrl:LoadingController,
    private AlertCntrl:AlertController
  ) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad SignupPage');
  }
  OnSignup(f:NgForm)
  {
    let email:string=f.value.email;
    let password:string=f.value.password;
    let Loader=this.LoadingCntrl.create({
      content:'Signing up ...'
    });
    Loader.present();
    this.AuthService.Signup(email,password)
    .then((data)=>{
      Loader.dismiss();
      console.log(data);
    })
    .catch((error)=>{
      Loader.dismiss();
      let alert = this.AlertCntrl.create({
        title: 'Signup failed !',
        subTitle: error.message,
        buttons: ['OK']
      });
      alert.present();
    });
    
  }

}
