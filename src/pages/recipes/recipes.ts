import { RecipePage } from './../recipe/recipe';
import { RecipeService } from './../../app/services/recipe';
import { Recipe } from './../../models/recipe';
import { EditRecipePage } from './../edit-recipe/edit-recipe';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-recipes',
  templateUrl: 'recipes.html',
})
export class RecipesPage  {
  Recipes:Recipe[];
  constructor(public navCtrl: NavController, public navParams: NavParams,private RecipeService:RecipeService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecipesPage');
  }

  OnNewRecipe()
  {
    this.navCtrl.push(EditRecipePage,{mode:'New'})
  }

  ionViewWillEnter()
  {
    this.Recipes=this.RecipeService.GetRecipesList();
  }

  OnLoadRecipe(recipe:Recipe,index:number)
  {
      this.navCtrl.push(RecipePage,{recipe:recipe,index:index});
  }
}
