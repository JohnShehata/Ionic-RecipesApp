import { Ingredient } from './../../models/ingredients';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { ShoppingListService } from '../../app/services/shoppinglist';

/**
 * Generated class for the ShoppingListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shopping-list',
  templateUrl: 'shopping-list.html',
})
export class ShoppingListPage implements OnInit {

  ShoppingListItems:Ingredient[]=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,private ShoppingListService:ShoppingListService) {
  }

  ionViewWillEnter() {
    this.LoadItems();
  }

  ngOnInit()
  {
    
  }
  OnAddItem(f:NgForm)
  {
    const itemname:string=f.value.IngredienName;
    const itemamount:number=f.value.IngredienAmount;
    this.ShoppingListService.AddItem(itemname,itemamount);
    f.reset();
    this.LoadItems();
  }

  private LoadItems()
  {
    this.ShoppingListItems=this.ShoppingListService.GetShoppingList();
  }

  RemoveItem(item:Ingredient)
  {
    console.log(item)
    this.ShoppingListService.RemoveItem(item);
    this.LoadItems();
  }
}
