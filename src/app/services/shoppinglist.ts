import { Ingredient } from './../../models/ingredients';
export class ShoppingListService
{
    private Ingredients:Ingredient[]=[];

    AddItem(name:string,Amount:number)
    {
        this.Ingredients.push(new Ingredient(name,Amount));
    }
    AddItems(Items:Ingredient[])
    {
            this.Ingredients.push(...Items);
    }
    RemoveItem(Item:Ingredient)
    {
        const Index=this.Ingredients.findIndex((itemElm)=>{
                return Item.name==itemElm.name;
        })
        this.Ingredients.splice(Index,1);
    }

    GetShoppingList()
    {
        return this.Ingredients.slice();
    }
}