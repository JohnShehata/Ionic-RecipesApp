import { Recipe } from './../../models/recipe';
import { Ingredient } from './../../models/ingredients';
import { RecipeService } from './../../app/services/recipe';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, AlertController, ToastController } from 'ionic-angular';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-edit-recipe',
  templateUrl: 'edit-recipe.html',
})
export class EditRecipePage implements OnInit {
  Recipe:Recipe;
  index:number;
  Mode:string='New';
  Difficulties:string[]=['Easy','Medium','Hard']
  RecipeForm:FormGroup;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private actionSheetCtrl:ActionSheetController,
    private AlertCntrl:AlertController,
    private ToastrCntrl:ToastController,
    private RecipeService:RecipeService
  ) {}

  ngOnInit()
  {
    this.Mode=this.navParams.get('mode');
    console.log('Mode :'+ this.Mode)
    if(this.Mode=='Edit')
    {
      this.Recipe=this.navParams.get('recipe');
      this.index=this.navParams.get('index');
    }
    this.InitializeForm();
    
  }


  private InitializeForm()
  {
    let title=null;
    let description=null;
    let difficulty='Medium';
    let ingredients=[];

    if(this.Mode=='Edit')
    {

      title=this.Recipe.title;
      description=this.Recipe.description;
      difficulty=this.Recipe.difficulty;
      for(let ingredient of this.Recipe.ingredients)
      {
          ingredients.push(new FormControl(ingredient.name,Validators.required));
      }
    }
    
    this.RecipeForm=new FormGroup({
      'Title':new FormControl(title,Validators.required),
      'Description':new FormControl(description,Validators.required),
      'Difficulty':new FormControl('Medium',Validators.required),
      'Ingredients':new FormArray(ingredients)
    });
  }
  OnManageIngredients()
  {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'what do you want to do ?',
      buttons: [
        {
          text: 'Add Ingredient',
          handler: () => {
            this.CreateNewIngredientAlert().present();
          }
        },
        {
          text: 'Remove all ingredients',
          role: 'destructive',
          handler: () => {
            const FArray:FormArray=<FormArray>this.RecipeForm.get('Ingredients');
            let Len=FArray.length;
            if(Len>0)
            {
              for(let a=Len-1;a>=0;a--)
              FArray.removeAt(a);

              let toast = this.ToastrCntrl.create({
                message: 'All items were deleted',
                duration: 1000,
              });
              toast.present();
            }
           
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
        }
      ]
    });
    actionSheet.present();
  }
  OnSubmit()
  {
    let FormValue=this.RecipeForm.value;
    let Ingredients:Ingredient[]=[];
    if(FormValue.Ingredients!=null && FormValue.Ingredients.length>0)
    {
      Ingredients=FormValue.Ingredients.map((ing)=>{
        return new Ingredient(ing,1);
      })
    }
    if(this.Mode=='Edit')
    {
      this.RecipeService.UpdateRecipe(this.index,FormValue.Title,FormValue.Description,FormValue.Difficulty,Ingredients);
    }
    else
    {
      this.RecipeService.AddItem(FormValue.Title,FormValue.Description,FormValue.Difficulty,Ingredients);
    }
    this.RecipeForm.reset();
    this.navCtrl.popToRoot();
  }

  private CreateNewIngredientAlert()
  {
    let prompt = this.AlertCntrl.create({
      title: 'Add Ingredient',
      inputs: [
        {
          name: 'name',
          placeholder: 'Name'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Add',
          handler: data => {
            if(data.name==null || data.name.trim()=='' )
            {
              let toast = this.ToastrCntrl.create({
                message: 'Please enter a valid value',
                duration: 1000,
              });
              toast.present();
              return;
            }

            (<FormArray>this.RecipeForm.get('Ingredients')).push(new FormControl(data.name,Validators.required))
            let toast = this.ToastrCntrl.create({
              message: 'Item added',
              duration: 1000,
            });
            toast.present();
          }
        }
      ]
    });
    return prompt;
  }

}
