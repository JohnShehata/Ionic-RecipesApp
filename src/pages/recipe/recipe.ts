import { ShoppingListService } from './../../app/services/shoppinglist';
import { RecipeService } from './../../app/services/recipe';
import { EditRecipePage } from './../edit-recipe/edit-recipe';
import { Recipe } from './../../models/recipe';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-recipe',
  templateUrl: 'recipe.html',
})
export class RecipePage implements OnInit {
  recipe:Recipe;
  recipeindex:number;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private RecipeService:RecipeService,
    private ShoppingListService:ShoppingListService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecipePage');
  }

  ngOnInit()
  {
    this.recipe=this.navParams.get('recipe');
    this.recipeindex=this.navParams.get('index');
  }

  OnEditRecipe()
  {
    this.navCtrl.push(EditRecipePage,{mode:'Edit',recipe:this.recipe,index:this.recipeindex})
  }
  OnAddIngredients()
  {
      this.ShoppingListService.AddItems(this.recipe.ingredients);
  }
  OnDeleteRecipe()
  {
      this.RecipeService.RemoveItem(this.recipe);
      this.navCtrl.popToRoot();
  }

}
