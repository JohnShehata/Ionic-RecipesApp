import firebase from 'firebase';
export class AuthService
{
    Signup(email:string,password:string)
    {
        return firebase.auth().createUserWithEmailAndPassword(email,password);
    }
}